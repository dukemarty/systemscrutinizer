# SystemScrutinizer

A plugin-extendable framework for system check applications, which can be used to diagnose (and eventually automatically fix) system settings.
